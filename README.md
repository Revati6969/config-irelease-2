# Config Repository - IRELEASE specific - Dev Environment

## Strategy: 

Following strategy is followed to store configuration for all iGTB Digital applications, in repository.

`<repo>`-`<customer>`-`<env>` / `<branch>` / `<client_app_name>`.properties

Where,

* `<repo>`-`<customer>`-`<env>`  
name of the Git (Bitbucket) repository, storing all configurations specific to customer (implementation) for individual environments (dev, test, qa, prod)

* `<branch>`  
name of the branch/tag, representing release or feature to be deployed

* `<client_app_name>`  
unique name of the application, whose properties are to be stored,  
format as `<app_type>`-`<app_name>`  
where,  
    * `<app_type>`  
        * action = Action API Facade
        * ingestion = Data/State Ingestion Module
        * digital = Delivery tier components
    * `<app_name>`
        * liquidity
        * payments
        * limits, etc...

---

## Example : 

### Dev-Repo

|Repository|Branch/Tag|Properties|Remarks|
|----------|----------|----------|-------|
|config-bbl-**dev**|||All **Dev** specific configuration|
||release_1.1|||
|||application.properties|**Default** config for all applications|
|||action-sweeps.properties|**Sweeps Action API** specific configuration|
|||ingestion-sweeps.properties|**Sweeps Ingestion** specific configuration|
|||...properties||

  
### Prod-Repo

|Repository|Branch/Tag|Properties|Remarks|
|----------|----------|----------|-------|
|config-bbl-**prod**|||All **Prod** specific configuration|
||release_1.1|||
|||application.properties|**Default** config for all applications|
|||action-sweeps.properties|**Sweeps Action API** specific configuration|
|||ingestion-sweeps.properties|**Sweeps Ingestion** specific configuration|
|||...properties||


